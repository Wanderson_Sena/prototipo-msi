pragma solidity >=0.5.11;

contract BackupBlock {
    bytes transaction;

    function setTransaction(bytes memory y) public {
        transaction = y;
    }

    function getTransaction() public view returns (bytes memory) {
        return transaction;
    }
}