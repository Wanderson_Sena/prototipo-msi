import json
from web3 import Web3
from web3.contract import ConciseContract

# Prepara a conexão com o ganache, ele precisa estar aberto
ganache_url = "http://127.0.0.1:7545"
ganache_private_key = "a97ee3e4583e76cd117a1373f968e4bb85f2dd940a4cdeabc61bb04cceddea95"
ganache_web3 = Web3(Web3.HTTPProvider(ganache_url))
ganache_acct = ganache_web3.eth.account.privateKeyToAccount(ganache_private_key)

# Set up web3 connection with Ethereum Ropsten testnet
privateKey = '50b67f37c12c02dd7d9ba921e38853f1f7bdaf80fddbd37da3dad861003e579e'
ropsten_web3 = Web3(Web3.HTTPProvider("https://ropsten.infura.io/v3/4998bb597a5d4e00b7d14ab1e6ca5035"))
ropsten_acct = ropsten_web3.eth.account.privateKeyToAccount(privateKey)


# set contract abi and bytecode
backup_block_abi = json.loads('''[
	{
		"constant": true,
		"inputs": [],
		"name": "getTransaction",
		"outputs": [
			{
				"internalType": "bytes",
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes",
				"name": "y",
				"type": "bytes"
			}
		],
		"name": "setTransaction",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	}
]''')
backup_block_bytecode = "608060405234801561001057600080fd5b5061030f806100206000396000f3fe608060405234801561001057600080fd5b50600436106100365760003560e01c806359705f811461003b5780638850202f146100be575b600080fd5b610043610179565b6040518080602001828103825283818151815260200191508051906020019080838360005b83811015610083578082015181840152602081019050610068565b50505050905090810190601f1680156100b05780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b610177600480360360208110156100d457600080fd5b81019080803590602001906401000000008111156100f157600080fd5b82018360208201111561010357600080fd5b8035906020019184600183028401116401000000008311171561012557600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f82011690508083019250505050505050919291929050505061021b565b005b606060008054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156102115780601f106101e657610100808354040283529160200191610211565b820191906000526020600020905b8154815290600101906020018083116101f457829003601f168201915b5050505050905090565b8060009080519060200190610231929190610235565b5050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061027657805160ff19168380011785556102a4565b828001600101855582156102a4579182015b828111156102a3578251825591602001919060010190610288565b5b5090506102b191906102b5565b5090565b6102d791905b808211156102d35760008160009055506001016102bb565b5090565b9056fea265627a7a72315820520a2ce86a5b16588d4451bb1dcfcb7833d06f29b38f2672738e50e9c264c07f64736f6c634300050b0032"
backup_block_contract_addr = None

# Set local contract abi and bytecode
address_history_abi = json.loads('''[
	{
		"constant": true,
		"inputs": [],
		"name": "getBlock",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getAddress",
		"outputs": [
			{
				"internalType": "bytes",
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes",
				"name": "x",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "y",
				"type": "uint256"
			}
		],
		"name": "set",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	}
]''')
address_history_bytecode = "608060405234801561001057600080fd5b50610354806100206000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c80632e97766d1461004657806338cc4831146100645780636f713284146100e7575b600080fd5b61004e6101ac565b6040518082815260200191505060405180910390f35b61006c6101b6565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156100ac578082015181840152602081019050610091565b50505050905090810190601f1680156100d95780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6101aa600480360360408110156100fd57600080fd5b810190808035906020019064010000000081111561011a57600080fd5b82018360208201111561012c57600080fd5b8035906020019184600183028401116401000000008311171561014e57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f82011690508083019250505050505050919291929080359060200190929190505050610258565b005b6000600154905090565b606060008054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561024e5780601f106102235761010080835404028352916020019161024e565b820191906000526020600020905b81548152906001019060200180831161023157829003601f168201915b5050505050905090565b816000908051906020019061026e92919061027a565b50806001819055505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106102bb57805160ff19168380011785556102e9565b828001600101855582156102e9579182015b828111156102e85782518255916020019190600101906102cd565b5b5090506102f691906102fa565b5090565b61031c91905b80821115610318576000816000905550600101610300565b5090565b9056fea265627a7a72315820c200f5499165a6b88a2283c77402e6b2fe55b7cf5dd862e1e07b87661d6f0b2864736f6c634300050b0032"
address_history_contract_addr = None


# If public contract does not exist
if backup_block_contract_addr is None:
    #create contract drom ABI and Bytecode
    contract_to_deploy = ropsten_web3.eth.contract(abi=backup_block_abi, bytecode=backup_block_bytecode)
    # Create contrac transacton
    construct_txn = contract_to_deploy.constructor().buildTransaction({
        'from': ropsten_acct.address,
        'nonce': ropsten_web3.eth.getTransactionCount(ropsten_acct.address),
        'gas': 1728712,
        'gasPrice': ropsten_web3.toWei('21', 'gwei'),
        'chainId': 3}) #Chain id = 3 for deploy in ropsten testnet
    
    # sign and send transaction
    signed_transaction = ropsten_acct.signTransaction(construct_txn)
    # Submit the transaction that deploys the contract
    transaction_hash = ropsten_web3.eth.sendRawTransaction(signed_transaction.rawTransaction)
    # Wait for the transaction to be mined, and get the transaction receipt
    transactionx_receipt = ropsten_web3.eth.waitForTransactionReceipt(transaction_hash)
    # Set contract address
    backup_block_contract_addr = transactionx_receipt.contractAddress

# Create the contract instance with the newly-deployed address
backup_block_contract = ropsten_web3.eth.contract(
    address=backup_block_contract_addr,
    abi=backup_block_abi,
)

# If private contract does not exist
if address_history_contract_addr is None:
    #create contract drom ABI and Bytecode
    contract_to_deploy = ganache_web3.eth.contract(abi=address_history_abi, bytecode=address_history_bytecode)

    # Create contrac transacton
    construct_txn = contract_to_deploy.constructor().buildTransaction({
        'from': ganache_acct.address,
        'nonce': ganache_web3.eth.getTransactionCount(ganache_acct.address),
        'gas': 1728712,
        'gasPrice': ganache_web3.toWei('21', 'gwei'),
        'chainId': 3}) #Chain id = 3 for deploy in ropsten testnet
    
    # sign and send transaction
    signed_transaction = ganache_acct.signTransaction(construct_txn)
    # Submit the transaction that deploys the contract
    transaction_hash = ganache_web3.eth.sendRawTransaction(signed_transaction.rawTransaction)
    # Wait for the transaction to be mined, and get the transaction receipt
    transactionx_receipt = ganache_web3.eth.waitForTransactionReceipt(transaction_hash)
    # Set contract address
    address_history_contract_addr = transactionx_receipt.contractAddress

# Create the contract instance with the newly-deployed address
address_history_contract = ganache_web3.eth.contract(
    address=address_history_contract_addr,
    abi=address_history_abi,
)

# Get local ganache block
block = ganache_web3.eth.getBlock('latest')

# Backup last block
transaction = backup_block_contract.functions.setTransaction(Web3.toBytes(block['transactions'][0])).buildTransaction({
    'gas': 70000,
    'gasPrice': ropsten_web3.toWei('1', 'gwei'),
    'from': ropsten_acct.address,
    'nonce': ropsten_web3.eth.getTransactionCount(ropsten_acct.address)
    }) 

# sign and send transaction
signed_transaction = ropsten_acct.signTransaction(transaction)
# Submit the transaction that deploys the contract
transaction_hash = ropsten_web3.eth.sendRawTransaction(signed_transaction.rawTransaction)
# Wait for the transaction to be mined, and get the transaction receipt
backup_receipt = ropsten_web3.eth.waitForTransactionReceipt(transaction_hash)

# Display the resulting values
print("Public block transaction hash:")
print(backup_block_contract.functions.getTransaction().call())

# Save transaction data
transaction = address_history_contract.functions.set(backup_receipt.transactionHash, backup_receipt.blockNumber).buildTransaction({
    'gas': 90000,
    'gasPrice': ganache_web3.toWei('1', 'gwei'),
    'from': ganache_acct.address,
    'nonce': ganache_web3.eth.getTransactionCount(ganache_acct.address)
    }) 

# sign and send transaction
signed_transaction = ganache_acct.signTransaction(transaction)
# Submit the transaction that deploys the contract
transaction_hash = ganache_web3.eth.sendRawTransaction(signed_transaction.rawTransaction)
# Wait for the transaction to be mined, and get the transaction receipt
backup_receipt = ganache_web3.eth.waitForTransactionReceipt(transaction_hash)

print("Private block number and transaction hash:")
print(address_history_contract.functions.getBlock().call())
print(address_history_contract.functions.getAddress().call())