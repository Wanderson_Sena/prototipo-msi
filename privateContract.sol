pragma solidity >=0.5.11;

contract AddressHistory {
    bytes addressHash;
    uint blockNumber;

    function set(bytes memory x, uint y) public {
        addressHash = x;
        blockNumber = y;
    }

    function getAddress() public view returns (bytes memory) {
        return addressHash;
    }

    function getBlock() public view returns (uint) {
        return blockNumber;
    }

}
